import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import descriptions from '../../../assets/photos/descriptions.json';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  public pageTitle: string[] = [];
  public path: string;
  public amount: number = 0;
  public modalFilePath: string = '';
  public showModal: boolean = false;
  public desc: any;
  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.path = params.id;
      this.pageTitle = [];
      this.pageTitle.push(this.path.substr(0, this.path.indexOf('_')));
      this.pageTitle.push(this.path.substr(this.path.indexOf('_') + 1));
      this.amount = Object.keys(descriptions[this.path]).length;
      this.desc = descriptions[this.path];
    });
  }

  toggleModal(i: number): void {
    this.modalFilePath = this.getFilePath(i);
    this.showModal = !this.showModal;
  }

  getFilePath(i: number): string {
    return `/assets/photos/${this.path}/${i+1}.png`;
  }

  preventDefault(event: any) {
    event.preventDefault();
    event.stopPropagation();
  }

}
